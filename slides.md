# How to route a packet to Mars

#### [@jaschoepfer@fediscience.org](https://fediscience.org/@jaschoepfer)

---

## Who Am I?

 - "jaschop"
  - Johann A. Schöpfer
  - Informatics Graduate
  - Developer, Researcher
  - someday Dr in space networking?

---

## Overview
#### OSI Layer Model

<img src="./images/osi-model.png" alt="OSI 7-layer model" width="25%"/>

--

## Overview
#### CCSDS protocol ecosystem

<img src="./images/ccsds-protocol-ecosystem.png" alt="CCSDS protocol ecosystem" width="60%"/>

--

## Overview
#### "bent-pipe" Networking (TDRS)

<img src="./images/tdrs_3rd_gen.jpg" width="30%"/>

---

## Licklider Transmission Protocol

 - context
   - overbooked DSN
   - long light delay
 - motivation: optimal bandwidth usage

--

## LTP

<img src="./images/licklider.png" width="60%"/>

--

## Licklider Transmission Protocol

- many blocks awaiting ACK in parallel
- optimistic timeouts avoid false positives
- still single-hop!

---

## Bundle Protocol
#### What is DTN?

 - Delay/Disruption-Tolerant Networking
 - no continuous link availability
 - solution: passive transmit/custody transfer

--

## Bundle Protocol
#### Use Case

<img src="./images/SINSIM-composition.png" width="60%"/>

--

## Bundle Protocol
#### Contact Graph Routing

<img src="./images/CGR-explainer.png" width="40%"/>

--

## Bundle Protocol
#### Routing Improvement

 - Bundle Fragmentation
 - Congestion Control
 - Future-Proofing Research

---

## Simulation Research
#### Code you can look at

 - Licklider implementation in ns-3 simulator
 - ION BP implementation by NASA
 - ...please answer my emails

--

## Simulation Research
#### The Framework I want

<img src="./images/SINSIM-catalogue.png" width="60%"/>

--

## Simulation Research
#### mission-compose.yml

<img src="./images/SINSIM-composition-full.png" width="60%"/>

---

## Shoutouts

 - C3SpaceCentre (c3space.de)
 - Libre Space Foundation (libre.space)
 - other cool space talks at #cccamp23nortx