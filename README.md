# slides-cccamp23

Slides for my [Talk](https://pretalx.c3voc.de/camp2023/talk/7FHFZA/) at Chaos Communication Camp 2023.

## reveal.js

Built with [reveal.js](https://github.com/hakimel/reveal.js)!

Installation method: download release and unzip as `./reveal.js`
